import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, RequestOptions } from '@angular/http';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { AppComponent } from './app.component';
import { EventDetailsComponent } from './event/event-details/event-details.component';

import { routing } from './app.routing';

import { Event } from './shared/event';
import { Organization } from './shared/organization';
import { EventsService } from './shared/events.service';
import { CitiesService } from './shared/cities.service';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './home/navbar/navbar.component';
import { JumbotronComponent } from './home/jumbotron/jumbotron.component';
import { AthleteLoginComponent } from './home/athlete-login/athlete-login.component';
import { OrganizationLoginComponent } from './home/organization-login/organization-login.component';
import { FilterComponent } from './home/filter/filter.component';
import { Section01Component } from './home/section01/section01.component';
import { MainEventComponent } from './home/section01/main-event/main-event.component';
import { LoginBoxComponent } from './home/section01/login-box/login-box.component';
import { Section02Component } from './home/section02/section02.component';
import { EventsComponent } from './home/section02/events/events.component';
import { LoginService } from './shared/login.service';
import { AuthRequestOptions } from './shared/auth-requests';
import { AllEventsComponent } from './home/section01/all-events/all-events.component';
import { FooterComponent } from './home/footer/footer.component';
import { FooterHeaderComponent } from './home/footer/footer-header/footer-header.component';
import { FooterBottomComponent } from './home/footer/footer-bottom/footer-bottom.component';
import { NewsFormComponent } from './home/footer/footer-header/news-form/news-form.component';
import { EventClockPayComponent } from './shared/event-clock-pay/event-clock-pay.component';
import { MapComponent } from './shared/map/map.component';
import { ButtonWishlistInscriptionComponent } from './shared/button-wishlist-inscription/button-wishlist-inscription.component';
import { ButtonFacebookSponsersComponent } from './shared/button-facebook-sponsers/button-facebook-sponsers.component';
import { NavEventDetailsComponent } from './shared/nav-event-details/nav-event-details.component';
import { EventRelatedComponent } from './shared/event-related/event-related.component';
import { EventModalitiesComponent } from './shared/event-modalities/event-modalities.component';
import { FormContactComponent } from './shared/form-contact/form-contact.component';
import { MainLoginComponent } from './home/main-login/main-login.component';
import { PlaceDropdownComponent } from './home/filter/place-dropdown/place-dropdown.component';
import { DateDropdownComponent } from './home/filter/date-dropdown/date-dropdown.component';
import { PageFilterComponent } from './page-filter/page-filter.component';
import { ModeDropdownComponent } from './home/filter/mode-dropdown/mode-dropdown.component';
import { TypeDropdownComponent } from './home/filter/type-dropdown/type-dropdown.component';
import { WishlistIconComponent } from './shared/wishlist-icon/wishlist-icon.component';
import { WishlistEventsIconComponent } from './shared/wishlist-events-icon/wishlist-events-icon.component';
// import { MainFilterComponent } from './home/filter/main-filter/main-filter.component';
import { OrganizationRegisterComponent } from './organization-register/organization-register.component';
import { AthleteRegisterComponent } from './athlete-register/athlete-register.component';
import { ProfileOrganizerComponent } from './profile-organizer/profile-organizer.component';

// import {SimpleNotificationsModule} from 'angular2-notifications';
import { EventFormComponent } from './profile-organizer/event-form/event-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MyEventsComponent } from './profile-organizer/my-events/my-events.component';
import { MenuProfileOrganizerComponent } from './profile-organizer/menu-profile-organizer/menu-profile-organizer.component';
import { InscriptionsComponent } from './profile-organizer/inscriptions/inscriptions.component';
import { MyDataComponent } from './profile-organizer/my-data/my-data.component';
import { AccountsComponent } from './profile-organizer/accounts/accounts.component';
import { ModalNewAccountComponent } from './profile-organizer/modal-new-account/modal-new-account.component';
import { MyInscriptionsComponent } from './profile-athlete/my-inscriptions/my-inscriptions.component';
import { MenuProfileAthletesComponent } from './profile-athlete/menu-profile-athletes/menu-profile-athletes.component';
import { NavProfileAthleteComponent } from './profile-athlete/nav-profile-athlete/nav-profile-athlete.component';
import { DataAthleteComponent } from './profile-athlete/data-athlete/data-athlete.component';
import { FinancialComponent } from './profile-athlete/financial/financial.component';
import { EventsHistoryComponent } from './profile-athlete/events-history/events-history.component';
import { PaginationComponent } from './shared/pagination/pagination.component';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { OrganizerService } from './shared/organizer.service';
import { SearchFilterPipe } from './shared/search-filter-pipe';
import { ShareButtonModule } from '@ngx-share/button';
import { HttpClientModule } from '@angular/common/http';
import { NextEventsComponent } from './next-events/next-events.component';
import { Modality } from './shared/modality';
import { ModalityService } from './shared/modality.service';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';


@NgModule({
  declarations: [
    AppComponent,
    EventDetailsComponent,
    HomeComponent,
    NavbarComponent,
    JumbotronComponent,
    AthleteLoginComponent,
    LoginBoxComponent,
    OrganizationLoginComponent,
    FilterComponent,
    Section01Component,
    Section02Component,
    MainEventComponent,
    EventsComponent,
    AllEventsComponent,
    FooterComponent,
    FooterHeaderComponent,
    FooterBottomComponent,
    NewsFormComponent,
    EventClockPayComponent,
    MapComponent,
    ButtonWishlistInscriptionComponent,
    ButtonFacebookSponsersComponent,
    NavEventDetailsComponent,
    EventRelatedComponent,
    EventModalitiesComponent,
    FormContactComponent,
    MainLoginComponent,
    PlaceDropdownComponent,
    DateDropdownComponent,
    PageFilterComponent,
    ModeDropdownComponent,
    TypeDropdownComponent,
    WishlistIconComponent,
    WishlistEventsIconComponent,
    // MainFilterComponent,
    OrganizationRegisterComponent,
    AthleteRegisterComponent,
    ProfileOrganizerComponent,
    EventFormComponent,
    MyEventsComponent,
    MenuProfileOrganizerComponent,
    InscriptionsComponent,
    MyDataComponent,
    AccountsComponent,
    ModalNewAccountComponent,
    MyInscriptionsComponent,
    MenuProfileAthletesComponent,
    NavProfileAthleteComponent,
    DataAthleteComponent,
    FinancialComponent,
    EventsHistoryComponent,
    PaginationComponent,
    SearchFilterPipe,
    NextEventsComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    MatTabsModule,
    MatChipsModule,
    // SimpleNotificationsModule.forRoot(),
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    PaginationModule.forRoot(),
    Ng2AutoCompleteModule,
    SnotifyModule,
    CarouselModule.forRoot(),
    HttpClientModule,      // (Required) for share counts
    ShareButtonModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  providers: [EventsService, LoginService, { provide: RequestOptions, useClass: AuthRequestOptions }, CitiesService, OrganizerService, { provide: 'SnotifyToastConfig', useValue: ToastDefaults}, SnotifyService, ModalityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
