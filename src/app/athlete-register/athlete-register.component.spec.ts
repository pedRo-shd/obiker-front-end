import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AthleteRegisterComponent } from './athlete-register.component';

describe('AthleteResgisterComponent', () => {
  let component: AthleteRegisterComponent;
  let fixture: ComponentFixture<AthleteRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AthleteRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AthleteRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
