import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { LoginService } from '../shared/login.service';
import {SnotifyService} from 'ng-snotify';

@Component({
  selector: 'app-athlete-register',
  templateUrl: './athlete-register.component.html',
  styleUrls: ['./athlete-register.component.scss']
})
export class AthleteRegisterComponent implements OnInit {

  constructor(private loginService: LoginService,
              private route: ActivatedRoute,
              private router: Router,
              private snotifyService: SnotifyService) { }

  ngOnInit() {
  }

  athleteSignUp(form: NgForm) {
    const name = form.value.reg_athlete_name;
    const email = form.value.reg_athlete_email;
    const cpf = form.value.reg_athlete_cpf;
    const gender = form.value.reg_athlete_gender;
    const birth_date = form.value.reg_athlete_birth_date;
    const avatar = form.value.reg_athlete_avatar;
    const password = form.value.reg_athlete_password;
    const password_confirmation = form.value.reg_athlete_password_confirmation;
    this.loginService.athleteSignUp(name,
                                    email,
                                    gender,
                                    cpf,
                                    birth_date,
                                    avatar,
                                    password,
                                    password_confirmation)
      .subscribe(
        data => {
          this.loginService.setToken((data.json().auth_token));
          this.loginService.setCurrentAthlete();
          this.router.navigateByUrl('profile-athlete/my-inscriptions');
          this.snotifyService.success('Login efetuado com sucesso!',
            {
              timeout: 6000,
              closeOnClick: true,
              position: 'rightTop'
            });
        },
        (error) => {this.snotifyService.error('Dados inválidos, tenta novamente!',
          {
            timeout: 6000,
            closeOnClick: true,
            position: 'rightTop'
          });
        }
      );
  }

}
