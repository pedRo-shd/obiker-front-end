import { Component, OnInit } from '@angular/core';
import { OrganizerService } from '../../shared/organizer.service';

@Component({
  selector: 'app-inscriptions',
  templateUrl: './inscriptions.component.html',
  styleUrls: ['./inscriptions.component.scss']
})
export class InscriptionsComponent implements OnInit {
  organizerData = [];
  athletes = [];
  subscriptions = [];

  constructor(private organizerService: OrganizerService) { }

  ngOnInit() {
    this.getAthletes();
  }

  getData() {
    this.organizerService.organizerData()
      .subscribe(data => {
        this.organizerData = data;
        this.organizerData = Array.of(this.organizerData);
        this.subscriptions = data.memberships;
      }
    );
  }

  getAthletes() {
    this.organizerService.getAthletes()
      .subscribe(data => {
        this.athletes = data;
        this.athletes = Array.of(this.athletes);
      }
    );
  }

}
