import { Component, OnInit } from '@angular/core';
import { OrganizerService } from '../../shared/organizer.service';
import { Event } from '../../shared/event';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.scss']
})
export class MyEventsComponent implements OnInit {

  public events: Event[] = [];
  public base_url: any;
  subscriptions = [];
  titalIncription = '';
  organizerData = [];

  month = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez'
  ];
  constructor(private organizerService: OrganizerService) { }

  ngOnInit() {
    this.organizerService.getEventsOrganizer()
      .subscribe(data => {
        this.events = data;
        this.base_url = environment.api_base_url
      }
    );

    this.organizerService.organizerData()
      .subscribe(data => {
        this.organizerData = data;
        this.organizerData = Array.of(this.organizerData);
      }
    );
  }

  countMembership(event_id) {
    let quant = 0;
    this.subscriptions.forEach((subs) => {
      if ( subs.event_id === event_id ) {
        quant ++;
      }
    });
    return quant;
  }

}
