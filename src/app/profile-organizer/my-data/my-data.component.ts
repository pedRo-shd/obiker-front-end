import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Organization } from '../../shared/organization';
import { OrganizerService } from '../../shared/organizer.service';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';
import {SnotifyService} from 'ng-snotify';

@Component({
  selector: 'app-my-data',
  templateUrl: './my-data.component.html',
  styleUrls: ['./my-data.component.scss']
})
export class MyDataComponent implements OnInit {

  public organization: Organization;
  public editForm: FormGroup;
  public img: any;

  constructor(
    private organizerService: OrganizerService,
    public fb: FormBuilder,
    private router: Router,
    private SnotifyService: SnotifyService,
    private route: ActivatedRoute
  ) {
      this.editForm = this.fb.group({
      name: ["", Validators.required],
      email: ["", Validators.required],
      birthday: "",
      gender: "",
      phone: ["", Validators.required],
      photo: "",
    });
   }

  ngOnInit() {
    this.organizerService.organizerData()
      .subscribe(data => {
        this.organization = new Organization(data);

        this.editForm = this.fb.group({
          name: [this.organization.name, Validators.required],
          email: [this.organization.email, Validators.required],
          birthday: "",
          gender: "",
          phone: [this.organization.phone, Validators.required],
          photo: "",
          // country: [this.organization.address.country, Validators.required],
          // state: [this.organization.address.state, Validators.required],
          // city: [this.organization.address.city, Validators.required],
        });
      }
    );
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.img = myReader.result;
    };
     myReader.readAsDataURL(file);
  }

  fileChange(event) {
    this.readThis(event.target);
  }

  save(){
    this.organizerService.update(this.editForm.value, this.img)
      .subscribe(res => {
        this.router.navigate(['/profile-organizer/my-events']);
        this.SnotifyService.success('Os dados informados no formulário foram atualizados!', 'Dados atualizados com sucesso',
          {
            timeout: 6000,
            closeOnClick: true,
            position: 'rightTop',
            titleMaxLength: 200,
            bodyMaxLength: 600
          }
        );
      }, error => {
        this.SnotifyService.error('Tenha certeza que todos os campos obrigatórios estão preenchidos, e tente novamente!', 'Falha ao cadastrar os dados',
          {
            timeout: 6000,
            closeOnClick: true,
            position: 'rightTop',
            titleMaxLength: 200,
            bodyMaxLength: 600
          }
        );

      }
    );
  }

}
