import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuProfileOrganizerComponent } from './menu-profile-organizer.component';

describe('MenuProfileOrganizerComponent', () => {
  let component: MenuProfileOrganizerComponent;
  let fixture: ComponentFixture<MenuProfileOrganizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuProfileOrganizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuProfileOrganizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
