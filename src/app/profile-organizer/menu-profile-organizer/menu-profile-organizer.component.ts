import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';
import { OrganizerService } from '../../shared/organizer.service';
import { Organization} from '../../shared/organization';
import { Router } from '@angular/router';
import {SnotifyService} from 'ng-snotify';
import { LoginService } from '../../shared/login.service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-menu-profile-organizer',
  templateUrl: './menu-profile-organizer.component.html',
  styleUrls: ['./menu-profile-organizer.component.scss']
})
export class MenuProfileOrganizerComponent implements OnInit {

  public organization: Organization;

  // public editForm: FormGroup;
  public img: any;
  public base_url: any;

  // organizerData = [];
  // name = '';

  constructor(
    // public fb: FormBuilder,
    private OrganizerService: OrganizerService,
    private router: Router,
    private snotifyService: SnotifyService,
    private LoginService: LoginService
  ) {
  // this.editForm = this.fb.group({
      //   photo: "",
      // });
    }

  ngOnInit() {
    this.OrganizerService.organizerData()
      .subscribe(data => {
        this.organization = new Organization(data);
        if(this.organization.photo != null && this.organization.photo != ""){
          this.base_url = environment.api_base_url
          this.img = this.organization.photo.profile_thumb.url;
        }
      }
    );

    // this.getData();
    //
    // this.editForm = this.fb.group({
    //       name: [this.organization.name, Validators.required],
    //       photo: "",
    //     });
  }

  // getData() {
  //   this.OrganizerService.organizerData()
  //     .subscribe(data => {
  //       this.organizerData = data;
  //       this.organization = new Organization(data);
  //       if(this.organization.photo != null && this.organization.photo != ""){
  //         this.img = this.organization.photo.url;
  //       }
  //       this.organizerData = Array.of(this.organizerData);
  //     }
  //   );
  // }

  // Método para converter arquivo file para base_64
  // readThis(inputValue: any) : void {
  //   var file:File = inputValue.files[0];
  //   var myReader:FileReader = new FileReader();
  //
  //   myReader.onloadend = (e) => {
  //     this.img = myReader.result;
  //   };
  //    myReader.readAsDataURL(file);
  // }

  // Quando o usuario seleciona uma foto, cai nesse método fileChange
  // event.taget é para pegar o arquivo que o usuário subiu
  // fileChange(event) {
  //   this.readThis(event.target);
  // }
  //
  // save(){
  //   this.OrganizerService.update(this.editForm.value, this.img)
  //     .subscribe(res => {
  //       this.router.navigateByUrl('profile-organization/my-events');
  //       this.snotifyService.success('Dados alterados com sucesso!',
  //         {
  //           timeout: 6000,
  //           closeOnClick: true,
  //           position: 'rightTop'
  //         });
  //     }, error => {
  //         this.snotifyService.error('Não foi possível atualizar os dados, tente novamente!',
  //           {
  //             timeout: 6000,
  //             closeOnClick: true,
  //             position: 'rightTop'
  //           });
  //     }
  //   );
  // }
  organizationLogOut() {
    this.LoginService.organizationLogOut();
    this.snotifyService.success('Você não está mais logado em sua conta', 'Logout efetuado com sucesso',
      {
        timeout: 6000,
        closeOnClick: true,
        position: 'rightTop',
        titleMaxLength: 200,
        bodyMaxLength: 600
      });
  }
}
