import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../shared/login.service';

import {SnotifyService} from 'ng-snotify';

import { OrganizerService } from '../shared/organizer.service';


@Component({
  selector: 'app-profile-organizer',
  templateUrl: './profile-organizer.component.html',
  styleUrls: ['./profile-organizer.component.scss']
})
export class ProfileOrganizerComponent implements OnInit {
  organizerData = [];

  isCollapsed: boolean = true;

  collapsed(event: any): void {
    console.log(event);
  }

  expanded(event: any): void {
    console.log(event);
  }

  constructor(private route: ActivatedRoute,
              private router: Router,
              private loginService: LoginService,
              private organizerService: OrganizerService,
              private snotifyService: SnotifyService) { }

  ngOnInit() {

  }


  getData() {
    this.organizerService.organizerData()
      .subscribe(data => {
        this.organizerData = data;
        this.organizerData = Array.of(this.organizerData);
      }
    );
  }

  organizationLogOut() {
    this.loginService.organizationLogOut();
    this.snotifyService.success('Logout efetuado com sucesso!',
      {
        timeout: 6000,
        closeOnClick: true,
        position: 'rightTop'
      });
  }
}
