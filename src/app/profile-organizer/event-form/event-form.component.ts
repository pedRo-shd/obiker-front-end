import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Event } from '../../shared/event';
import { Modality } from '../../shared/modality';
import { EventsService } from '../../shared/events.service';
import { ModalityService } from '../../shared/modality.service';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';
import {SnotifyService} from 'ng-snotify';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {

  public event: Event;
  public modalities: Modality[] = [];
  public title: string = "";
  public eventForm: FormGroup;
  public img: any;
  public event_date: any;
  public subscription_end: any;
  public _output: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private EventsService: EventsService,
    public fb: FormBuilder,
    private snotifyService: SnotifyService,
    private ModalityService: ModalityService
  ) {

    this.eventForm = this.fb.group({
      name: ["", Validators.required],
      start_street: ["", Validators.required],
      finish_street: ["", Validators.required],
      start_number: "",
      finish_number: "",
      start_neighbor: ["", Validators.required],
      finish_neighbor: ["", Validators.required],
      start_city: ["", Validators.required],
      start_state: ["", Validators.required],
      event_date: ["", Validators.required],
      event_info: ["", Validators.required],
      photo: ["", Validators.required],
      kit_image: "",
      kit_description: "",
      contact_email: ["", Validators.required],
      contact_phone: "",
      start_hours: ["", Validators.required],
      modality_id: ["", Validators.required],
      // status: ["", Validators.required],
      rules: "",
      subscription_start: "",
      subscription_end: ["", Validators.required],
      max_subscriptions: ["", Validators.required],
      price: ["", Validators.required],
    });


  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.title = params['id'] ? 'Editar Evento' : 'Cadastrar Evento';
      if (params['id']){
        this.EventsService.getEvent(params['id'])
          .subscribe(
            data => {
              this.event =  new Event(data.event);
              if(this.event.photo != null && this.event.photo != ""){
                this.img = this.event.photo.url;
              }

              this.eventForm = this.fb.group({
                name: [this.event.name, Validators.required],
                start_street: [this.event.start_street, Validators.required],
                finish_street: [this.event.finish_street, Validators.required],
                start_number: "",
                finish_number: "",
                start_neighbor: [this.event.start_neighbor, Validators.required],
                finish_neighbor: [this.event.finish_neighbor, Validators.required],
                start_city: [this.event.start_city, Validators.required],
                start_state: [this.event.start_state, Validators.required],
                event_date: [this.event.event_date, Validators.required],
                event_info: [this.event.event_info, Validators.required],
                photo: [this.event.photo, Validators.required],
                kit_image: "",
                kit_description: [this.event.kit_description, Validators.required],
                contact_email: [this.event.contact_email, Validators.required],
                contact_phone: "",
                start_hours: [this.event.start_hours, Validators.required],
                modality_id: [this.event.modality_id, Validators.required],
                // status: [this.event.// status, Validators.required],
                rules: "",
                subscription_start: "",
                subscription_end: [this.event.subscription_end, Validators.required],
                max_subscriptions: [this.event.max_subscriptions, Validators.required],
                price: [this.event.price, Validators.required],
              });
            },
            err => {
              this.router.navigateByUrl('/'),
              this.snotifyService.error('Ocorreu um erro ao carregar o formulÃ¡rio, tente novamente!', 'NÃ£o foi possÃ­vel carregar o formulÃ¡rio',
              {
                timeout: 6000,
                closeOnClick: true,
                position: 'rightTop',
                titleMaxLength: 200,
                bodyMaxLength: 600
              });
            }
          );
      }
    });

    this.ModalityService.getModalities()
      .subscribe(data => {
        this.modalities = data;
      }
    );
  }

  readThis(inputValue: any) : void {
    var file:File = inputValue.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.img = myReader.result;
    };
     myReader.readAsDataURL(file);
  }

  fileChange(event) {
    this.readThis(event.target);
  }

  save(){
    if(this.event && this.event.id)
    {
      this.EventsService.updateEvent(this.eventForm.value, this.img, this.event.id)
        .subscribe(res => {
          this.router.navigateByUrl('profile-organizer/my-events');
          this.snotifyService.success(
            'Os dados informados no formulÃ¡rio foram atualizados!', 'Dados atualizados com sucesso',
            {
              timeout: 6000,
              closeOnClick: true,
              position: 'rightTop',
              titleMaxLength: 200,
              bodyMaxLength: 600
            }
          );
        }, error => {
          this.snotifyService.error(
            'Os dados nÃ£o foram atualizados, tente novamente!', 'Falha ao atualizar os dados',
            {
              timeout: 6000,
              closeOnClick: true,
              position: 'rightTop',
              titleMaxLength: 200,
              bodyMaxLength: 600
            }
          );
        }
      );
    }
    else{
      this.EventsService.addEvent(this.eventForm.value, this.img)
        .subscribe(res => {
          this.router.navigateByUrl('profile-organizer/my-events');
          this.snotifyService.success(
            'Após a aprovação do Obiker, seu evento será publicado!', 'Evento cadastrado com sucesso',
            {
              timeout: 6000,
              closeOnClick: true,
              position: 'rightTop',
              titleMaxLength: 200,
              bodyMaxLength: 600
            }
          );
        }, error => {
          this.snotifyService.error(
            'Tenha certeza que todos os campos obrigatórios estãoo preenchidos, e tente novamente!', 'Falha ao cadastrar o evento',
            {
              timeout: 6000,
              closeOnClick: true,
              position: 'rightTop',
              titleMaxLength: 200,
              bodyMaxLength: 600
            }
          );
        }
      );
    }

  }

}
