import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Event } from '../../shared/event';
import { EventsService } from '../../shared/events.service';
import { Modality } from '../../shared/modality';
import { Organization } from '../../shared/organization';
import { environment } from '../../../environments/environment';

// import { ShareButtons } from '@ngx-share/core';


@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit {

  constructor(private _eventsService: EventsService, private route: ActivatedRoute, private router: Router) { }


    public event: Event;
    public organization: Organization;
    private name: any;
    private start_street: any;
    private start_number: any;
    private start_neighbor: any;
    private start_city: any;
    private start_state: any;
    private event_date: any;
    private event_info: any;
    private start_hours: any;
    public photo: any;
    private contact_email: any;
    private contact_phone: any;
    private modality: Modality [] = [];
    private rules: any;
    private kit_image: any;
    private kit_description: any;
    private subscription_start: any;
    private subscription_end: any;
    private max_subscriptions: any;
    private days_to_go_event: any;
    private hours_to_go_event: any;
    public base_url: any;

  month = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez'
  ];

  ngOnInit() {
    this.route.params.subscribe(params => {
      this._eventsService.getOrganizationEvent(params['id']).subscribe(
        data => {
          this.organization = data;
          this.name = data.organization.name;
        },
         err => {console.log(err)}
      );

      this._eventsService.getEvent(params['id']).subscribe(
        data => {
          this.event = data;
          this.base_url = environment.api_base_url;
          this.name = data.event.name;
          this.start_street = data.event.start_street;
          this.start_number = data.event.start_number;
          this.start_neighbor = data.event.start_neighbor;
          this.start_city = data.event.start_city;
          this.start_state = data.event.start_state;
          this.start_hours = data.event.start_hours;
          this.event_date = data.event.event_date;
          this.event_info = data.event.event_info;
          this.photo = data.event.photo;
          this.contact_email = data.event.contact_email;
          this.contact_phone = data.event.contact_phone;
          this.modality = data.event.modality;
          this.rules = data.event.rules;
          this.kit_image = data.event.kit_image;
          this.kit_description = data.event.kit_description;
          this.subscription_start = data.event.subscription_start;
          this.subscription_end = data.event.subscription_end;
          this.max_subscriptions = data.event.max_subscriptions;
        },
        err => {console.log(err)}
      );
    });
  }

}
