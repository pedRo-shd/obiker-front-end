import { Event } from './event';

export class Organization {
  id: number;
  name: string;
  email: string;
  birthday: string;
  gender: string;
  phone: string;
  photo: any;

  constructor(organizationInfo:any) {
    this.id = organizationInfo.id;
    this.name = organizationInfo.name;
    this.email = organizationInfo.email;
    this.birthday = organizationInfo.birthday;
    this.gender = organizationInfo.gender;
    this.phone = organizationInfo.phone;
    this.photo = organizationInfo.photo;
  }
}
