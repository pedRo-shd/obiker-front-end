import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavEventDetailsComponent } from './nav-event-details.component';

describe('NavEventDetailsComponent', () => {
  let component: NavEventDetailsComponent;
  let fixture: ComponentFixture<NavEventDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavEventDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavEventDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
