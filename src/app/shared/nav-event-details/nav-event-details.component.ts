import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-nav-event-details',
  templateUrl: './nav-event-details.component.html',
  styleUrls: ['./nav-event-details.component.scss']
})
export class NavEventDetailsComponent implements OnInit {
  isLogged = false;
  athleteLogged = false;
  organizationLogged = false;
  isCollapsed: boolean = true;

  collapsed(event: any): void {
    console.log(event);
  }

  expanded(event: any): void {
    console.log(event);
  }

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.isServiceLogged();
    this.athleteServiceLogged();
    this.organizationServiceLogged();
  }

  isServiceLogged() {
    this.isLogged = this.loginService.isLogged();
    console.log('Log result = ' + this.isLogged);
  }

  athleteServiceLogged() {
    this.athleteLogged = this.loginService.athleteLogged();
    console.log('Log Athlete = ' + this.athleteLogged);
  }


  organizationServiceLogged() {
    this.organizationLogged = this.loginService.organizationLogged();
    console.log('Log Organization = ' + this.organizationLogged);
  }

  athleteLogOut() {
    this.loginService.athleteLogOut();
  }
  organizationLogOut() {
    this.loginService.organizationLogOut();
  }
}
