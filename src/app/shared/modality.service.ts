import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

import { Modality } from './modality';
import { environment } from '../../environments/environment';

@Injectable()
export class ModalityService {

  constructor(private http: Http) { }

  getModalities(){
    return this.http.get(environment.api_base_url + 'modalities')
      .map(res => res.json());
  }

}
