import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WishlistEventsIconComponent } from './wishlist-events-icon.component';

describe('WishlistEventsIconComponent', () => {
  let component: WishlistEventsIconComponent;
  let fixture: ComponentFixture<WishlistEventsIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WishlistEventsIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WishlistEventsIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
