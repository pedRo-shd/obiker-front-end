import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventRelatedComponent } from './event-related.component';

describe('EventRelatedComponent', () => {
  let component: EventRelatedComponent;
  let fixture: ComponentFixture<EventRelatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventRelatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventRelatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
