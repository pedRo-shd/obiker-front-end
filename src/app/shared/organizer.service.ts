import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

import { Event } from './event';
import { LoginService } from '../shared/login.service';
import { environment } from '../../environments/environment';


@Injectable()
export class OrganizerService {

  constructor(private http: Http, private loginService: LoginService) { }

  autocomplete(){
    return this.http.get(environment.api_base_url + '/autocomplete')
      .map(res => res.json());
  }

  searchEvents(params){
	let parameters = new URLSearchParams();
	for(var f in params) { parameters.set(f, params[f]) }

	return this.http.get(environment.api_base_url + '/search', {search: parameters})
  	.map(res => res.json());
  }

  getEventsOrganizer() {
    return this.http.get(environment.api_base_url + 'events')
      .map(res => res.json());
    }

  organizerData() {
    return this.http.get(environment.api_base_url + '/organizations/organization')
      .map(
        (response: Response) => {
          const data = response.json();
          return data;
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw(error);
        }
      );
  }

  getAthletes() {
    return this.http.get(environment.api_base_url + '/get_all_subscriptions')
      .map(
        (response: Response) => {
          const data = response.json();
          console.log(data);
          return data;
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw(error);
        }
      );
  }

  update(params, photo){
    return this.http.put(environment.api_base_url + 'organizations/',
     {
       "name": params.name,
       "email": params.email,
       "birthday": params.birthday,
       "gender": params.gender,
       "phone": params.phone,
        photo: photo
      }
    ).map(res => res.json());
  }
}
