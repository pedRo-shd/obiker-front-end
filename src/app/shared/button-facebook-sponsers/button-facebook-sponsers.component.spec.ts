import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonFacebookSponsersComponent } from './button-facebook-sponsers.component';

describe('ButtonFacebookSponsersComponent', () => {
  let component: ButtonFacebookSponsersComponent;
  let fixture: ComponentFixture<ButtonFacebookSponsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonFacebookSponsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonFacebookSponsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
