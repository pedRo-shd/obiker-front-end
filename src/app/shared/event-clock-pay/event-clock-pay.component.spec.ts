import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventClockPayComponent } from './event-clock-pay.component';

describe('EventClockPayComponent', () => {
  let component: EventClockPayComponent;
  let fixture: ComponentFixture<EventClockPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventClockPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventClockPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
