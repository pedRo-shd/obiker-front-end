import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EventsService } from '../../shared/events.service';

@Component({
  selector: 'app-event-clock-pay',
  templateUrl: './event-clock-pay.component.html',
  styleUrls: ['./event-clock-pay.component.scss']
})
export class EventClockPayComponent implements OnInit {

  constructor(private _eventsService: EventsService, private route: ActivatedRoute,
              private router: Router) { }

  // days_to_go_event: '';
  public days_to_go_event: string[] = [''];
  public hours_to_go_event: string[] = [''];
  public minutes_to_go_event: string[] = [''];
  private _output: any;


  ngOnInit() {
    this.route.params.subscribe(params => {
      this._eventsService.getDaysToGo(params['id']).subscribe(
        data => {
          this.days_to_go_event = data;
        },
         err => {console.log(err)}
      );

      this._eventsService.getHoursToGo(params['id']).subscribe(
        data => {

          this.hours_to_go_event = data;
          {console.log(data)};
          this._output = data;

        },
         err => {console.log(err)}
      );

      this._eventsService.getMinutesToGo(params['id']).subscribe(
        data => {
          this.minutes_to_go_event = data;
        },
         err => {console.log(err)}
      );

    });
  }

}
