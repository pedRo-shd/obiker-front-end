export class Contact {

  id: number;
  name: string;
  email: string;
  title: string;
  description: string;
  event_id: number;

  constructor(contactInfo:any) {
    this.id = contactInfo.id;
    this.name = contactInfo.name;
    this.email = contactInfo.email;
    this.title = contactInfo.title;
    this.description = contactInfo.description;
    this.event_id = contactInfo.event_id;
  }
}
