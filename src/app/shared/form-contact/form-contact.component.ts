import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';
import { EventsService } from '../../shared/events.service';
import { Contact } from '../../shared/contact';
import {SnotifyService} from 'ng-snotify';


@Component({
  selector: 'app-form-contact',
  templateUrl: './form-contact.component.html',
  styleUrls: ['./form-contact.component.scss']
})
export class FormContactComponent implements OnInit {

  public contact: Contact;
  public contactForm: FormGroup;

  constructor(
    private _eventsService: EventsService,
    private route: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder,
    private snotifyService: SnotifyService
  ) { this.contactForm = this.fb.group({
        name: ["", Validators.required],
        email: ["", Validators.required],
        title: ["", Validators.required],
        description: ["", Validators.required],
      });
    }

  ngOnInit() {
    // Passar o id do evento
  }

  save(){
    this._eventsService.addEventContacts(this.contactForm.value)
      .subscribe(res => {
        this.snotifyService.success('Informações enviadas com sucesso, logo você receberá informações via e-mail!',
          {
            timeout: 6000,
            closeOnClick: true,
            position: 'rightTop'
          });
      },
      err => {
        this.snotifyService.error('Não foi possível enviar as informações do formulário, tente novamente!',
        {
          timeout: 6000,
          closeOnClick: true,
          position: 'rightTop'
        });
      }
    );
  }

}
