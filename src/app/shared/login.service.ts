import { Injectable, OnInit } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class LoginService implements OnInit {
  currentAthlete = false;
  currentOrganization = false;
  auth_token = false;
  private url = environment.api_base_url;

  constructor(private http: Http) { }
  ngOnInit() {
    this.isLogged()
    this.athleteLogged();
    this.organizationLogged();
  }

  organizationSignUp(name: string,
                    email: string,
                    street: string,
                    number: string,
                    neighbor: string,
                    city: string,
                    state: string,
                    bank_code: string,
                    bank_agency: string,
                    bank_agency_dv: string,
                    bank_account: string,
                    bank_account_dv: string,
                    cpf: string,
                    bank_account_name: string,
                    bank_document: string,
                    password: string,
                    password_confirmation: string): Observable<any> {
                    const headers = new Headers({'Content-Type': 'application/json'});
    const body = { name,
                  email,
                  street,
                  number,
                  neighbor,
                  city,
                  state,
                  bank_code,
                  bank_agency,
                  bank_agency_dv,
                  bank_account,
                  bank_account_dv,
                  cpf,
                  bank_account_name,
                  bank_document,
                  password,
                  password_confirmation };
    return this.http.post(environment.api_base_url + '/organization_signup',
                          body,
                          {headers: headers});
  }

  athleteSignUp(name: string,
                    email: string,
                    gender: string,
                    cpf: string,
                    birth_date: string,
                    avatar: string,
                    password: string,
                    password_confirmation: string): Observable<any> {
                    const headers = new Headers({'Content-Type': undefined});
    const body = { name,
                  email,
                  gender,
                  cpf,
                  birth_date,
                  avatar,
                  password,
                  password_confirmation };
                return this.http.post(environment.api_base_url + '/athlete_signup',
                          body,
                          {headers: headers});
                }

  organizationLogin(email: string, password: string): Observable<any> {
    // this.credentials = [email, password];
    const headers = new Headers({'Content-Type': 'application/json'});
    const body = { email, password };
    return this.http.post(environment.api_base_url + '/organization_auth/login',
                          body,
                          {headers: headers});
  }

  athleteLogin(email: string, password: string): Observable<any> {
    // this.credentials = [email, password];
    const headers = new Headers({'Content-Type': 'application/json'});
    const body = { email, password };
    return this.http.post(environment.api_base_url + '/athlete_auth/login',
                          body,
                          {headers: headers});
  }

  setToken(token: string): void {
    localStorage.setItem('auth_token', token);
  }

  setCurrentAthlete(): void {
    localStorage.setItem('current_athlete', '1');
  }

  setCurrentOrganization(): void {
    localStorage.setItem('current_organization', '1');
  }

  athleteLogOut() {
    localStorage.clear();
  }

  organizationLogOut() {
    localStorage.clear();
  }

  // getToken(): string {
  //   this.auth_token = localStorage.getItem('auth_token');
  //   return this.auth_token;
  // }
  // getCurrentAthlete(): string {
  //   this.currentAthlete = localStorage.getItem('current_athlete');
  //   return this.currentAthlete;
  // }
  // getCurrentOrganization(): string {
  //   this.currentOrganization = localStorage.getItem('current_organization');
  //   return this.currentOrganization;
  // }

  isLogged() {
    return this.auth_token = localStorage.getItem('auth_token') != null;
  }

  athleteLogged() {
    return this.currentAthlete = localStorage.getItem('current_athlete') != null;
  }

  organizationLogged() {
    return this.currentOrganization = localStorage.getItem('current_organization') != null;
  }


}
