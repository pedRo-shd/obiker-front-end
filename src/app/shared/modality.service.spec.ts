import { TestBed, inject } from '@angular/core/testing';

import { ModalityService } from './modality.service';

describe('ModalityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalityService]
    });
  });

  it('should be created', inject([ModalityService], (service: ModalityService) => {
    expect(service).toBeTruthy();
  }));
});
