import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonWishlistInscriptionComponent } from './button-wishlist-inscription.component';

describe('ButtonWishlistInscriptionComponent', () => {
  let component: ButtonWishlistInscriptionComponent;
  let fixture: ComponentFixture<ButtonWishlistInscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonWishlistInscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonWishlistInscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
