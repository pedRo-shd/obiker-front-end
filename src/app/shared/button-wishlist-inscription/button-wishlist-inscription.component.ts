import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../shared/login.service';
import {SnotifyService} from 'ng-snotify';

@Component({
  selector: 'app-button-wishlist-inscription',
  templateUrl: './button-wishlist-inscription.component.html',
  styleUrls: ['./button-wishlist-inscription.component.scss']
})
export class ButtonWishlistInscriptionComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private snotifyService: SnotifyService, private _loginService: LoginService) { }

  isLogged = false;
  athleteLogged = false;
  organizationLogged = false;


  ngOnInit() {
    this.isServiceLogged();
    this.athleteServiceLogged();
    this.organizationServiceLogged();
  }

  isServiceLogged() {
    this.isLogged = this._loginService.isLogged();
    console.log('Log result = ' + this.isLogged);
  }

  athleteServiceLogged() {
    this.athleteLogged = this._loginService.athleteLogged();
    console.log('Log Athlete = ' + this.athleteLogged);
  }

  organizationServiceLogged() {
    this.organizationLogged = this._loginService.organizationLogged();
    console.log('Log Organization = ' + this.organizationLogged);
  }

  onSubmit() {
    this.router.navigateByUrl('/athlete-login');
    this.snotifyService.warning('É necessário se logar como Atleta para inscrever-se no evento!', 'Faça o Login ou Cadatre-se',
      {
        timeout: 6000,
        closeOnClick: true,
        position: 'rightTop',
        titleMaxLength: 200,
        bodyMaxLength: 600
      });
  }

  inSubmit() {
    this.snotifyService.error('Este evento ainda não esta liberado para a lista de desejos!', 'Não foi possível adicionar aos favoritos',
      {
        timeout: 6000,
        closeOnClick: true,
        position: 'rightTop',
        titleMaxLength: 200,
        bodyMaxLength: 600
      });
  }

}
