import { Event } from '../shared/event';

export class Modality {
  name: string;
  picture: any;
  // events: Event [] = [];

  constructor(modalityInfo:any) {
    this.name = modalityInfo.name;
    this.picture = modalityInfo.picture;

    // this.events = new Event(modalityInfo.events);
  }
}
