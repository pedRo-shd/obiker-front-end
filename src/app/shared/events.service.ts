import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { AuthRequestOptions } from '../shared/auth-requests';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

import { Event } from './event';
import { LoginService } from '../shared/login.service';
import { environment } from '../../environments/environment';


@Injectable()
export class EventsService {

  constructor(private http: Http, private loginService: LoginService) { }

  autocomplete(){
    return this.http.get(environment.api_base_url + 'autocomplete')
      .map(res => res.json());
  }

  searchEvents(params){
	let parameters = new URLSearchParams();
	for(var f in params) { parameters.set(f, params[f]) }

	return this.http.get(environment.api_base_url + 'search', {search: parameters})
  	.map(res => res.json());
  }

  getEvents() {
    return this.http.get(environment.api_base_url + 'get_events')
      .map(
        (response: Response) => {
          const data = response.json();
          return data;
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong');
        }
      );
  }

  getLastEvent() {
    return this.http.get(environment.api_base_url + 'get_events')
      .map(
        (response: Response) => {
          const data = response.json();
          return data[data.length - 1];
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Something went wrong');
        }
      );
  }

  getEvent(id){
    return this.http.get(environment.api_base_url + 'events/' + id)
      .map(res => res.json());
  }

  getOrganizationEvent(id){
    return this.http.get(environment.api_base_url + 'organization_event/' + id)
      .map(res => res.json());
  }

  getDaysToGo(id){
    return this.http.get(environment.api_base_url + 'day_to_go/' + id)
    .map(res => res.json());
  }

  getHoursToGo(id){
    return this.http.get(environment.api_base_url + 'hours_to_go/' + id)
      .map(res => res.json());
  }

  getMinutesToGo(id){
    return this.http.get(environment.api_base_url + 'minutes_to_go/' + id)
      .map(res => res.json());
  }

  // Método na API, não condiz com a feature (ver depois)
  // updateCloneEvent(id){
  //   return this.http.put(environment.api_base_url + 'clone_event' + id)
  //     .map(res => res.json());
  // }

  addEvent(event, event_image){
    // var modality_params = {
      // name: event.modality.name,
      // picture: event.picture,
    // }

    return this.http.post(environment.api_base_url + 'events/',
      {
        name: event.name,
        start_street: event.start_street,
        finish_street: event.finish_street,
        start_number: event.start_number,
        finish_number: event.finish_number,
        start_neighbor: event.start_neighbor,
        finish_neighbor: event.finish_neighbor,
        start_city: event.start_city,
        start_state: event.start_state,
        event_date: event.event_date,
        event_info: event.event_info,
        photo: event_image,
        kit_description: event.kit_description,
        contact_email: event.contact_email,
        contact_phone: event.contact_phone,
        start_hours: event.start_hours,
        modality_id: event.modality_id,
        closed: event.closed,
        rules: event.rules,
        subscription_start: event.subscription_start,
        subscription_end: event.subscription_end,
        max_subscriptions: event.max_subscriptions,
        price: event.price
        // "modality_attributes": modality_params
      }
    )
      .map(res => res.json());
  }

  updateEvent(event, picture, id){
    var modality_params = {
      name: event.name,
      // picture: event.picture,
    }

    return this.http.put(environment.api_base_url + 'events/' + id,
      {
        name: event.name,
        start_street: event.start_street,
        finish_street: event.finish_street,
        start_number: event.start_number,
        finish_number: event.finish_number,
        start_neighbor: event.start_neighbor,
        finish_neighbor: event.finish_neighbor,
        start_city: event.start_city,
        start_state: event.start_state,
        event_date: event.event_date,
        event_info: event.event_info,
        picture: event.picture,
        kit_description: event.kit_description,
        contact_email: event.contact_email,
        contact_phone: event.contact_phone,
        start_hours: event.start_hours,
        modality: event.modality,
        closed: event.closed,
        rules: event.rules,
        subscription_start: event.subscription_start,
        subscription_end: event.subscription_end,
        max_subscriptions: event.max_subscriptions,
        price: event.price,
        "modality_attributes": modality_params
      }
    )
      .map(res => res.json());
  }


  deleteEvent(id){
    return this.http.delete(environment.api_base_url + 'events/' + id)
      .map(res => res.json());
  }

  getEventsCarousel(){
    return this.http.get(environment.api_base_url + 'get_events_carousel')
      .map(res => res.json());
  }

  addEventContacts(contact){
    return this.http.post(environment.api_base_url + 'contacts/',
      {
        event_id: contact.event_id,
        name: contact.name,
        email: contact.email,
        title: contact.title,
        description: contact.description
      }
    ).map(res => res.json());
  }

  nextEvents(){
    return this.http.get(environment.api_base_url + 'next_events')
      .map(res => res.json());
  }

  // joinEvent(id athlete_id){
  //   return this.http.put(environment.api_base_url + 'events' + id + athlete_id)
  //     .map(res => res.json());
  // }

}
