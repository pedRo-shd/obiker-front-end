import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';


@Injectable()
export class CitiesService {

  constructor(private http: Http) { }

  citiesBR(){
    return this.http.get(environment.api_base_url + '/cities', {})
      .map(res => res.json());
  }

  statesBR(){
    return this.http.get(environment.api_base_url + '/states')
      .map(res => res.json());
  }

}
