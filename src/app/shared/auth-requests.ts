import { Headers, Http, BaseRequestOptions } from '@angular/http';
import { LoginService } from '../shared/login.service';
import { OrganizerService } from '../shared/organizer.service';

const AUTH_HEADER_KEY = 'Authorization';

export class AuthRequestOptions extends BaseRequestOptions {
  constructor() {
    super();
    const token = localStorage.getItem('auth_token');
      if (token) {
        this.headers.append(AUTH_HEADER_KEY, token);
      }
  }
}
