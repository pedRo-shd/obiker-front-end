import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventModalitiesComponent } from './event-modalities.component';

describe('EventModalitiesComponent', () => {
  let component: EventModalitiesComponent;
  let fixture: ComponentFixture<EventModalitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventModalitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventModalitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
