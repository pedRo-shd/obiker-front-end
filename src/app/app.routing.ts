import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { EventDetailsComponent } from './event/event-details/event-details.component';
import { PageFilterComponent } from './page-filter/page-filter.component';
import { OrganizationRegisterComponent } from './organization-register/organization-register.component';
import { AthleteRegisterComponent } from './athlete-register/athlete-register.component';
import { AthleteLoginComponent } from './home/athlete-login/athlete-login.component';
import { OrganizationLoginComponent } from './home/organization-login/organization-login.component';
// import { ProfileOrganizerComponent } from './profile-organizer/profile-organizer.component';
import { EventFormComponent } from './profile-organizer/event-form/event-form.component';
import { MyEventsComponent } from './profile-organizer/my-events/my-events.component';
import { InscriptionsComponent } from './profile-organizer/inscriptions/inscriptions.component';
import { MyDataComponent } from './profile-organizer/my-data/my-data.component';
import { AccountsComponent } from './profile-organizer/accounts/accounts.component';
import { MyInscriptionsComponent } from './profile-athlete/my-inscriptions/my-inscriptions.component';
import { DataAthleteComponent } from './profile-athlete/data-athlete/data-athlete.component';
import { FinancialComponent } from './profile-athlete/financial/financial.component';
import { EventsHistoryComponent } from './profile-athlete/events-history/events-history.component';
import { AllEventsComponent } from './home/section01/all-events/all-events.component';
import { NextEventsComponent } from './next-events/next-events.component';

// Cria nossas Rotas
const appRoutes: Routes = [
  { path: '', pathMatch: 'full', component: HomeComponent },
  { path: 'page-filter', component: PageFilterComponent },
  { path: 'organization-register', component: OrganizationRegisterComponent },
  { path: 'organization-login', component: OrganizationLoginComponent },
  { path: 'athlete-register', component: AthleteRegisterComponent },
  { path: 'athlete-login', component: AthleteLoginComponent },
  // { path: 'profile-organizer', component: ProfileOrganizerComponent },
  { path: 'profile-organizer/event-form/new', component: EventFormComponent },
  { path: 'profile-organizer/event-form/:id/edit', component: EventFormComponent },
  { path: 'profile-organizer/my-events', component: MyEventsComponent },
  { path: 'profile-organizer/inscriptions', component: InscriptionsComponent },
  { path: 'profile-organizer/my-data', component: MyDataComponent },
  { path: 'profile-organizer/accounts', component: AccountsComponent },
  { path: 'profile-athlete/my-inscriptions', component: MyInscriptionsComponent },
  { path: 'profile-athlete/data-athlete', component: DataAthleteComponent },
  { path: 'profile-athlete/financial', component: FinancialComponent },
  { path: 'profile-athlete/events-history', component: EventsHistoryComponent },
  { path: 'all-events', component: AllEventsComponent },
  { path: 'event/:id', component: EventDetailsComponent },
  { path: 'next-events', component: NextEventsComponent },
];

// Exporta a constante routing para importar no arquivo app.module.ts
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
