import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../shared/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import {SnotifyService} from 'ng-snotify';

@Component({
  selector: 'app-jumbotron',
  templateUrl: './jumbotron.component.html',
  styleUrls: ['./jumbotron.component.scss']
})
export class JumbotronComponent implements OnInit {

  isLogged = false;
  athleteLogged = false;
  organizationLogged = false;

  constructor(private loginService: LoginService, private route: ActivatedRoute, private router: Router, private snotifyService: SnotifyService) {
  }
  ngOnInit() {
    this.isServiceLogged();
    this.athleteServiceLogged();
    this.organizationServiceLogged();
  }

  isServiceLogged() {
    this.isLogged = this.loginService.isLogged();
    console.log('Log result = ' + this.isLogged);
  }

  athleteServiceLogged() {
    this.athleteLogged = this.loginService.athleteLogged();
    console.log('Log Athlete = ' + this.athleteLogged);
  }

  organizationServiceLogged() {
    this.organizationLogged = this.loginService.organizationLogged();
    console.log('Log Organization = ' + this.organizationLogged);
  }

  onSubmit() {
    this.snotifyService.error('É necessário se logar para cadastrar um evento!', 'Faça o Login ou Cadatre-se',
      {
        timeout: 6000,
        closeOnClick: true,
        position: 'rightTop',
        titleMaxLength: 200,
        bodyMaxLength: 600
      });
  }
}
