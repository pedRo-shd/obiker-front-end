import { Component, OnInit } from '@angular/core';
import { Event } from '../shared/event';
import { EventsService } from '../shared/events.service';

import { NavbarComponent } from './navbar/navbar.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { AthleteLoginComponent } from './athlete-login/athlete-login.component';
import { OrganizationLoginComponent } from './organization-login/organization-login.component';
import { Section01Component } from './section01/section01.component';
import { MainEventComponent } from './section01/main-event/main-event.component';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private events: Event[] = [];
  private event: Event[] = [];
  private _output: any;

  constructor(private eventsService: EventsService) { }


  ngOnInit() {
    this.eventsService.getLastEvent()
      .subscribe(data => {
        this.event = data;
      }
    );
  }
}
