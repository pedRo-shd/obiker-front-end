import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {SnotifyService} from 'ng-snotify';


@Component({
  selector: 'app-all-events',
  templateUrl: './all-events.component.html',
  styleUrls: ['./all-events.component.scss']
})
export class AllEventsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private snotifyService: SnotifyService) { }

  ngOnInit() {

  }

}
