import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { CarouselConfig } from 'ngx-bootstrap/carousel';

import { Event } from '../../../shared/event';
import { EventsService } from '../../../shared/events.service';
import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-main-event',
  templateUrl: './main-event.component.html',
  styleUrls: ['./main-event.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 3000, noPause: true, showIndicators: true } }
  ]
})
export class MainEventComponent implements OnInit {

  public events: Event [] = [];
  // public eventsService: EventsService;
  eventDay = '';
  eventMonth = '';
  lastEvent = [];
  public base_url: any;
  public start_hours: any;
  public _output: any;

  month = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez'
  ];

  constructor(private eventsService: EventsService) {
   }

  ngOnInit() {
      this.eventsService.getEventsCarousel()
        .subscribe(data => {
          this.events = data;
          this.base_url = environment.api_base_url;
          this._output = data;
        }
      );
    // this.getLastEvent();

  }
  // getLastEvent() {
  //   this.eventsService.getLastEvent()
  //     .subscribe(
  //       data => {
  //         this.lastEvent = (data);
  //         this.lastEvent = Array.of(this.lastEvent),
  //         this.eventDay = data['event_date'].split('-')[2],
  //         this.eventMonth = this.month[ (data['event_date'].split('-')[1]) - 1 ],
  //         this.start_hours = this.start_hours
  //       },
  //       error => console.log(error)
  //     );
  // }

}
