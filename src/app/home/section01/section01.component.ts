import { Component, OnInit } from '@angular/core';
import { EventsService } from '../../shared/events.service';

import { LoginBoxComponent } from './login-box/login-box.component';
import { MainEventComponent } from './main-event/main-event.component';
import { AllEventsComponent } from './all-events/all-events.component';

@Component({
  selector: 'app-section01',
  templateUrl: './section01.component.html',
  styleUrls: ['./section01.component.scss']
})
export class Section01Component implements OnInit {
  private event: Event[] = [];

  constructor(private eventsService: EventsService) { }

  ngOnInit() {
    this.eventsService.getLastEvent()
    .subscribe(data => {
      this.event = data;
    }
  );
  }

}
