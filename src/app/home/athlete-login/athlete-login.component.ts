import * as $ from 'jquery';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { LoginService } from '../../shared/login.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {SnotifyService} from 'ng-snotify';


@Component({
  selector: 'app-athlete-login',
  templateUrl: './athlete-login.component.html',
  styleUrls: ['./athlete-login.component.scss']
})
export class AthleteLoginComponent implements OnInit {
  @ViewChild('athleteWindow') public modal: ModalDirective;

  constructor(private loginService: LoginService,
              private route: ActivatedRoute,
              private router: Router,
              private snotifyService: SnotifyService) { }

  ngOnInit() {
  }

  athleteLogin(form: NgForm) {
    const email = form.value.athlete_email;
    const password = form.value.athlete_password;
    this.loginService.athleteLogin(email, password)
      .subscribe(
        data => {
          this.loginService.setToken((data.json().auth_token));
          this.loginService.setCurrentAthlete();
          this.router.navigateByUrl('profile-athlete/my-inscriptions');
          this.snotifyService.success('Login efetuado com sucesso!',
            {
              timeout: 6000,
              closeOnClick: true,
              position: 'rightTop'
            });
        },
        err => {this.snotifyService.error('Login ou senha inválidos, tente novamente!', 'Dados inválidos',
          {
            timeout: 6000,
            closeOnClick: true,
            position: 'rightTop'
          });
        }
      );
  }
}
