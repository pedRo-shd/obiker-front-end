import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Event } from '../../../shared/event';
import { EventsService } from '../../../shared/events.service';
import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  events = [];
  public base_url: any;

  month = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez'
  ];

  constructor(private EventsService: EventsService, private route: ActivatedRoute) { }

  ngOnInit() {
      this.getEvents();
  }

  getEvents() {
    this.EventsService.getEvents()
      .subscribe(data => {
        this.events = data;
        this.base_url = environment.api_base_url;
      }
    );
  }

}
