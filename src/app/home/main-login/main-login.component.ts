import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { AthleteLoginComponent } from '../athlete-login/athlete-login.component';
import { OrganizationLoginComponent } from '../organization-login/organization-login.component';

import { LoginService } from '../../shared/login.service';

@Component({
  selector: 'app-main-login',
  templateUrl: './main-login.component.html',
  styleUrls: ['./main-login.component.scss']
})
export class MainLoginComponent implements OnInit {

  constructor( private loginService: LoginService ) { }

  ngOnInit() {
  }

  organizationLogin(form: NgForm) {
    const email = form.value.organization_email;
    const password = form.value.organization_password;
    this.loginService.organizationLogin(email, password)
      .subscribe(
        data => this.loginService.setToken((data.json().auth_token)),
        (error) => console.log(error)
      );
    console.log('Submissio OK!!');
  }

  athleteLogin(form: NgForm) {
    const email = form.value.athlete_email;
    const password = form.value.athlete_password;
    this.loginService.athleteLogin(email, password)
      .subscribe(
        data => this.loginService.setToken((data.json().auth_token)),
        (error) => console.log(error)
      );
    console.log('Submissio OK!!');
  }
}
