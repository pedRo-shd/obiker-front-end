import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { LoginService } from '../../shared/login.service';
import {SnotifyService} from 'ng-snotify';

@Component({
  selector: 'app-organization-login',
  templateUrl: './organization-login.component.html',
  styleUrls: ['./organization-login.component.scss']
})
export class OrganizationLoginComponent implements OnInit {

  constructor(private loginService: LoginService,
              private route: ActivatedRoute,
              private router: Router,
              private snotifyService: SnotifyService) { }

  ngOnInit() {
  }
  organizationLogin(form: NgForm) {
    const email = form.value.organization_email;
    const password = form.value.organization_password;
    this.loginService.organizationLogin(email, password)
      .subscribe(
        data => {
          this.loginService.setToken((data.json().auth_token));
          this.loginService.setCurrentOrganization();
          this.router.navigateByUrl('profile-organizer/my-events');
          this.snotifyService.success('Login efetuado com sucesso!',
            {
              timeout: 6000,
              closeOnClick: true,
              position: 'rightTop'
            });
        },
        err => {this.snotifyService.error('Login ou senha inválidos, tente novamente!', 'Dados inválidos',
          {
            timeout: 6000,
            closeOnClick: true,
            position: 'rightTop'
          });
        }
      );
  }

}
