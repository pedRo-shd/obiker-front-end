// import { Component, OnInit } from '@angular/core';
// import { FilterComponent } from '../filter.component';
// import {Router, ActivatedRoute, Params} from '@angular/router';
// import { Http, URLSearchParams } from '@angular/http';
// import { EventsService } from '../../../shared/events.service';
//
//
// @Component({
//   selector: 'app-main-filter',
//   templateUrl: './main-filter.component.html',
//   styleUrls: ['./main-filter.component.scss']
// })
// export class MainFilterComponent implements OnInit {
//
//   private event: KeyboardEvent;
//   public query: string = "";
//   public arrayAutocomplete: string[] = [];
//   public searchModel = "";
//
//   constructor(private EventsService: EventsService, private router: Router, private route: ActivatedRoute) {}
//
//   ngOnInit() {
//     this.EventsService.autocomplete()
//       .subscribe(data => {
//         this.arrayAutocomplete = data;
//       }
//     );
//   }
//
//     public onEvent(event: KeyboardEvent): void {
//   	if(event.key == "Enter" && this.query != "")
//     	this.search();
//     }
//
//     search(){
//   	this.route.queryParams.subscribe(params => {
//     	let parameters = new URLSearchParams();
//     	for(var f in params) {
//       	if(f != "search")
//         	parameters.set(f, params[f])
//     	};
//     	this.router.navigateByUrl('page-filter?search=' + this.query + "&" + parameters.toString());
//   	});
//   }
//
// }
