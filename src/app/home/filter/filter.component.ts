import { Component, OnInit } from '@angular/core';
import { PlaceDropdownComponent } from './place-dropdown/place-dropdown.component';
import { DateDropdownComponent } from './date-dropdown/date-dropdown.component';
import { ModeDropdownComponent } from './mode-dropdown/mode-dropdown.component';
import { TypeDropdownComponent } from './type-dropdown/type-dropdown.component';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Http, URLSearchParams } from '@angular/http';
import { EventsService } from '../../shared/events.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  public shouldShow = false;
  public query: string = "";
  public arrayAutocomplete: string[] = [];
  public searchModel = "";

  constructor(
    private EventsService: EventsService, private router: Router, private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.EventsService.autocomplete()
      .subscribe(data => {
        this.arrayAutocomplete = data;
      }
    );
  }

  onSubmit() {
    if(this.query != "")
      this.search();
  }

  search(){
    this.route.queryParams.subscribe(params => {
      let parameters = new URLSearchParams();
      for(var f in params) {
        if(f != "search")
          parameters.set(f, params[f])
      };
      this.router.navigateByUrl('page-filter?search=' + this.query + "&" + parameters.toString());
    });
  }

  openDropdown() {
    this.shouldShow = !this.shouldShow;
  }

}
