import { Component, OnInit } from '@angular/core';
import {NewsFormComponent} from './news-form/news-form.component';

@Component({
  selector: 'app-footer-header',
  templateUrl: './footer-header.component.html',
  styleUrls: ['./footer-header.component.scss']
})
export class FooterHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
