import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../shared/login.service';

@Component({
  selector: 'app-nav-profile-athlete',
  templateUrl: './nav-profile-athlete.component.html',
  styleUrls: ['./nav-profile-athlete.component.scss']
})
export class NavProfileAthleteComponent implements OnInit {

  isCollapsed: boolean = true;

  collapsed(event: any): void {
    console.log(event);
  }

  expanded(event: any): void {
    console.log(event);
  }

  constructor(private loginService: LoginService) { }

  ngOnInit() {
  }

  athleteLogOut() {
    this.loginService.athleteLogOut();
  }

}
