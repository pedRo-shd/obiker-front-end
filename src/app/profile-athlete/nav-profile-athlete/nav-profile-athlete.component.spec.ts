import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavProfileAthleteComponent } from './nav-profile-athlete.component';

describe('NavProfileAthleteComponent', () => {
  let component: NavProfileAthleteComponent;
  let fixture: ComponentFixture<NavProfileAthleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavProfileAthleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavProfileAthleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
