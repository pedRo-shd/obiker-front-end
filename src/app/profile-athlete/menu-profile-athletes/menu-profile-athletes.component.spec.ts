import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuProfileAthletesComponent } from './menu-profile-athletes.component';

describe('MenuProfileAthletesComponent', () => {
  let component: MenuProfileAthletesComponent;
  let fixture: ComponentFixture<MenuProfileAthletesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuProfileAthletesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuProfileAthletesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
