import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Event } from '../shared/event';
import { EventsService } from '../shared/events.service';
import { Modality } from '../shared/modality';
import { ModalityService } from '../shared/modality.service';
import { Http, URLSearchParams } from '@angular/http';
import { CitiesService } from '../shared/cities.service';
import {SnotifyService} from 'ng-snotify';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
defineLocale('ptBr', ptBrLocale);

@Component({
  selector: 'app-page-filter',
  templateUrl: './page-filter.component.html',
  styleUrls: ['./page-filter.component.scss']
})
export class PageFilterComponent implements OnInit {

  public events: Event[] = [];
  public modalities: Modality[] = [];
  public begindate: string = "";
  public enddate: string = "";
  public category: string = "";
  public event_date: Date = new Date();
  public start_state: string = "";
  private start_city: string = "";
  public arrayAutocomplete: string[] = [];
  public searchModel = "";
  private cities_br: any;
  public states_br: any;
  private event: KeyboardEvent;
  public query: string = "";
  public _output: any;

  month = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez'
  ];
  //
  // bsValue: Date = new Date();
  // bsRangeValue: any = [new Date(2017, 7, 4), new Date(2017, 7, 20)];

  color: string;

  availableColors = [
    { name: 'none', color: '' },
  ];

  isCollapsed: boolean = true;
  istwoCollapsed: boolean = true;

  collapsed(event: any): void {
    console.log(event);
  }

  expanded(event: any): void {
    console.log(event);
  }

  constructor(
    private _citiesService: CitiesService,
    private EventsService: EventsService,
    private router: Router,
    private route: ActivatedRoute,
    private ModalityService: ModalityService,
    private snotifyService: SnotifyService
  ) { }


  ngOnInit() {
    this.EventsService.autocomplete()
      .subscribe(data => {
        this.arrayAutocomplete = data;
      }
    );
    // Método que pega os parâmetros da URL
    this.route
    .queryParams
    .subscribe(params => {

      // Método que chama nosso Service
      this.EventsService.searchEvents(params)
        .subscribe(data => this.events = data);
    });

    this._citiesService.citiesBR()
      .subscribe(data => {
        this.cities_br = data;
      }
    );

    this._citiesService.statesBR()
      .subscribe(data => {
        this.states_br = data;
      }
    );

    this.ModalityService.getModalities()
      .subscribe(data => {
        this.modalities = data;
      }
    );
  }

  public onEvent(event: KeyboardEvent): void {
  if(event.key == "Enter" && this.query != "")
    this.search();
  }

  search(){
    this.route.queryParams.subscribe(params => {
      let parameters = new URLSearchParams();
      for(var f in params) {
        if(f != "search")
          parameters.set(f, params[f])
      };
      this.router.navigateByUrl('page-filter?search=' + this.query + "&" + parameters.toString());
    });
  }

  filter(type, value){
	this.route.queryParams.subscribe(params => {
  	let parameters = new URLSearchParams();
  	for(var f in params) {
    	if(f != type)
      	parameters.set(f, params[f])
  	};
    	this.router.navigateByUrl('page-filter?' + type + "=" + value + "&" + parameters.toString());
  	});
  }


}
