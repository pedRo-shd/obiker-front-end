import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { LoginService } from '../shared/login.service';
import {SnotifyService} from 'ng-snotify';

@Component({
  selector: 'app-organization-register',
  templateUrl: './organization-register.component.html',
  styleUrls: ['./organization-register.component.scss']
})
export class OrganizationRegisterComponent implements OnInit {

  constructor(private loginService: LoginService, private route: ActivatedRoute,
              private router: Router, private snotifyService: SnotifyService) { }

  ngOnInit() {
  }
  organizationSignUp(form: NgForm) {
    const name = form.value.reg_organization_name;
    const email = form.value.reg_organization_email;
    const street = form.value.reg_organization_street;
    const number = form.value.reg_organization_number;
    const neighbor = form.value.reg_organization_neighbor;
    const city = form.value.reg_organization_city;
    const state = form.value.reg_organization_state;
    const bank = form.value.reg_organization_bank;
    const agency = form.value.reg_organization_agency;
    const agency_dv = form.value.reg_organization_agency_dv;
    const account = form.value.reg_organization_account;
    const account_dv = form.value.reg_organization_account_dv;
    const cpf = form.value.reg_organization_cpf;
    const account_name = form.value.reg_organization_account_name;
    const account_doc = form.value.reg_organization_account_doc;
    const password = form.value.reg_organization_password;
    const password_confirmation = form.value.reg_organization_password_confirmation;
    this.loginService.organizationSignUp(name,
                                        email,
                                        street,
                                        number,
                                        neighbor,
                                        city,
                                        state,
                                        bank,
                                        agency,
                                        agency_dv,
                                        account,
                                        account_dv,
                                        cpf,
                                        account_name,
                                        account_doc,
                                        password,
                                        password_confirmation)
      .subscribe(
        data => {
          this.loginService.setToken((data.json().auth_token));
          this.loginService.setCurrentOrganization();
          this.router.navigateByUrl('profile-organizer/my-data');
          this.snotifyService.success('Atualize seus dados para que o Obiker consiga contatá-lo se necessário', 'Cadastro efetuado com sucesso!',
            {
              timeout: 8000,
              closeOnClick: true,
              position: 'rightTop',
              titleMaxLength: 200,
              bodyMaxLength: 600
            });
        },
        (error) => {this.snotifyService.error('Tenha certeza que os campos obrigatórios estão preechidos, e tente novamente!', 'Não foi possível efetuar o cadastro',
          {
            timeout: 8000,
            closeOnClick: true,
            position: 'rightTop',
            titleMaxLength: 200,
            bodyMaxLength: 600
          });
        }
      );
  }

}
