import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Event } from '../shared/event';
import { EventsService } from '../shared/events.service';
import { Modality } from '../shared/modality';
import { ModalityService } from '../shared/modality.service';
import {SnotifyService} from 'ng-snotify';
import { Http, URLSearchParams } from '@angular/http';
import { CitiesService } from '../shared/cities.service';

@Component({
  selector: 'app-next-events',
  templateUrl: './next-events.component.html',
  styleUrls: ['./next-events.component.scss']
})
export class NextEventsComponent implements OnInit {

  public events: Event[] = [];
  public modalities: Modality[] = [];
  public begindate: string = "";
  public enddate: string = "";
  public category: string = "";
  public event_date: Date = new Date();
  private start_state: string = "";
  private start_city: string = "";
  public arrayAutocomplete: string[] = [];
  public searchModel = "";
  private cities_br: any;
  public states_br: any;
  private event: KeyboardEvent;
  public query: string = "";

  month = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez'
  ];

  isCollapsed: boolean = true;
  istwoCollapsed: boolean = true;

  collapsed(event: any): void {
    console.log(event);
  }

  expanded(event: any): void {
    console.log(event);
  }

  constructor(
    private ModalityService: ModalityService,
    private router: Router,
    private route: ActivatedRoute,
    private _eventsService: EventsService,
    private snotifyService: SnotifyService,
    private _citiesService: CitiesService,
  ) { }

  ngOnInit() {
    this._eventsService.nextEvents()
    .subscribe(
        data => {
        this.events = data;
        if(this.events.length <= 0) {
          this.router.navigateByUrl('/');
          this.snotifyService.error('não há próximos eventos disponíveis!', 'No momento',
          {
            timeout: 6000,
            closeOnClick: true,
            position: 'rightTop',
            titleMaxLength: 200,
            bodyMaxLength: 600
          })
        }
      }
    );

    this._eventsService.autocomplete()
      .subscribe(data => {
        this.arrayAutocomplete = data;
      }
    );

    this._citiesService.citiesBR()
      .subscribe(data => {
        this.cities_br = data;
      }
    );

    this._citiesService.statesBR()
      .subscribe(data => {
        this.states_br = data;
      }
    );

    this.ModalityService.getModalities()
      .subscribe(data => {
        this.modalities = data;
      }
    );
  }

  public onEvent(event: KeyboardEvent): void {
  if(event.key == "Enter" && this.query != "")
    this.search();
  }

  search(){
    this.route.queryParams.subscribe(params => {
      let parameters = new URLSearchParams();
      for(var f in params) {
        if(f != "search")
          parameters.set(f, params[f])
      };
      this.router.navigateByUrl('page-filter?search=' + this.query + "&" + parameters.toString());
    });
  }

  filter(type, value){
	this.route.queryParams.subscribe(params => {
  	let parameters = new URLSearchParams();
  	for(var f in params) {
    	if(f != type)
      	parameters.set(f, params[f])
  	};
    	this.router.navigateByUrl('page-filter?' + type + "=" + value + "&" + parameters.toString());
  	});
  }

}
