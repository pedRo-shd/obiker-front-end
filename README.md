# Obiker Client

This project is frontend interface of the OBiker

## Dependencies

Node: 9.4.0

Npm: 5.6.0

Angular CLI: 1.6.5

Angular : 5.2.1

Font-awesome Icons

run `npm install font-awesome --save`

NGX-Boostratp

run `npm install ngx-bootstrap --save`

Bootstrap 4

run `npm install bootstrap@4.0.0-beta.2 --save`

Material Angular

run `npm install @angular/material @angular/cdk --save`

run `npm install`

## Updating Angular CLI if necessary

run `npm uninstall -g angular-cli`

run `npm uninstall --save-dev angular-cli`

run `rm -rf node_modules dist`

run `npm cache clean --force`

run `npm install -g @angular/cli@latest`

run `npm install --save-dev @angular/cli@latest`

run `npm install`

## Development server

run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
